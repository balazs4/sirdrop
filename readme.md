# srdrop

> airdrop like clipboard transfering between iOS and pc

## server

```sh
deno run --allow-net --allow-sys --allow-write --allow-read https://gitlab.com/balazs4/srdrop/-/raw/main/main.js
# or
git clone https://gitlab.com/balazs4/srdrop.git && cd srdrop
deno task start
```

## client

download and install
https://www.icloud.com/shortcuts/ab29f2ca54264cbdabcb0c12771decdd
