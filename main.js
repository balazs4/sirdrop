import * as _std_path from "jsr:@std/path@1.0.8";
import * as _std_media_types from "jsr:@std/media-types@1.0.3";
// TODO: replace it with "jsr:@libs/qrcode@2.0.0" or similar
import qrcode from "https://deno.land/x/qrcode_terminal@v1.1.1/mod.js";

const encoder = new TextEncoder();
const nonce = crypto.randomUUID();

/** @type {Deno.ServeTcpOptions} */
const opts = {
  hostname: getlocalipv4(),
  port: 8000,
  onListen: function (srv) {
    const url = `http://${srv.hostname}:${srv.port}/${nonce}`;
    qrcode.generate(
      url,
      { small: true },
      /** @param {string} code*/ function (code) {
        Deno.stderr.write(encoder.encode(code));
        Deno.stderr.write(encoder.encode(url + "\n"));
      },
    );
  },
};

const server = Deno.serve(opts, async (request) => {
  if (
    request.method === "GET" && request.headers.get("x-ios-repeat") === "done"
  ) {
    queueMicrotask(server.shutdown);
    return new Response(undefined, { status: 202 });
  }

  Deno.stderr.writeSync(encoder.encode(`${request.method} ${request.url}\n`));
  for (const [key, value] of request.headers) {
    Deno.stderr.writeSync(encoder.encode(`${key}: ${value}\n`));
  }

  if (!request.url.includes(nonce)) {
    Deno.stderr.writeSync(encoder.encode(`missing or invalid nonce\n`));
    queueMicrotask(server.shutdown);
    return new Response(undefined, { status: 404 });
  }

  const contentType = request.headers.get("content-type");
  if (contentType === null) {
    Deno.stderr.writeSync(encoder.encode(`no content-type found\n`));
    queueMicrotask(server.shutdown);
    return new Response(undefined, { status: 400 });
  }

  if (!request.body) {
    Deno.stderr.writeSync(encoder.encode(`body is empty\n`));
    queueMicrotask(server.shutdown);
    return new Response(undefined, { status: 400 });
  }

  if ("application/json" === contentType) {
    const body = await request.json();
    const value = body["x-ios-clipboard"] || JSON.stringify(body);
    Deno.stdout.writeSync(encoder.encode(value + "\n"));
    return new Response(undefined, { status: 200 });
  }

  if ("text/plain" === contentType) {
    await request.body.pipeTo(Deno.stdout.writable);
    return new Response(undefined, { status: 200 });
  }

  const [writeable, filepath] = await outputFile(contentType);
  Deno.stdout.write(encoder.encode(filepath + "\n"));
  await request.body.pipeTo(writeable);
  return new Response(undefined, { status: 200 });
});

/** @returns {string|undefined} */
function getlocalipv4() {
  for (const inet of Deno.networkInterfaces()) {
    if (inet.name === "lo") continue;
    if (inet.family === "IPv6") continue;
    if (inet.address === "127.0.0.1") continue;
    return inet.address;
  }
}

/**
 * @param {string} contentType
 * @returns {Promise<[WritableStream<Uint8Array>, string]>}
 */
async function outputFile(contentType) {
  const ext = _std_media_types.extension(contentType) ||
    contentType.replace("/", "_");
  const now = new Date().toJSON().replace(/:/g, "");
  const filename = `ios-${now}.${ext}`;
  const filepath = _std_path.join(Deno.cwd(), filename);
  const file = await Deno.open(filepath, { createNew: true, write: true });
  return [file.writable, filepath];
}
